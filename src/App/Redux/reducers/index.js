import {combineReducers} from 'redux';
import productBook from './productBook';

const rootReducers= combineReducers({
 	productBook
});
export default rootReducers;