import React, { Component } from 'react';
import { getBook } from '../Redux/actions/index.js';
import { connect } from 'react-redux';

import Img from 'react-image'
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  root1: {
    alignItems: 'center',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));



function SimpleBottomNavigation() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  return (
    <BottomNavigation
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      showLabels
      className={classes.root1}>
      <BottomNavigationAction label="Recents" icon={<RestoreIcon />} />
      <BottomNavigationAction label="Favorites" icon={<FavoriteIcon />} />
      <BottomNavigationAction label="Nearby" icon={<LocationOnIcon />} />
    </BottomNavigation>
  );
}

function ButtonAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="Menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Book Zone
          </Typography>
          <Button color="inherit">Contact</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}


class Home extends Component {

   constructor(props){
    super(props);
    this.state ={ isLoading: true}
    }
    componentDidMount(){
        this.props.onBookNew()

    }
   

    render() {

      //const { bookNew } = this.props
        //alert(JSON.stringify(bookNew))
      

      return (
        <div>
        <Container fixed>
        <ButtonAppBar />
        <div style={{marginTop: 100}}>
        
            <Grid container style={{ flexGrow: 1}} >
            <Grid item xs={12}>
              <Grid container style={{ flexGrow: 1 ,alignItems:  'center', alignContent:  'center', justifyContent:  'center'}}  spacing={16}>
                {this.props.bookNew.map(value => (
                  <Grid key={value._id} item>
                 
                      <Card style={{width: 250, height: 400, margin: 10 ,alignItems:  'center', alignContent:  'center', justifyContent:  'center' }}>
                        
                               <CardActionArea>
                                <CardMedia
                                  style={{ height: 250,}}
                                  image={value.productImage}
                                  title="Contemplative Reptile"
                                />
                                
                              </CardActionArea>   
                              <CardContent>
                                  <Typography numberOfLines={1} gutterBottom variant="h6" component="h2">
                                    {value.name}
                                  </Typography>
                                  <Typography variant="body2" color="textSecondary" component="p">
                                    Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
                                    across all continents except Antarctica
                                  </Typography>
                                </CardContent>                    
                      </Card>


                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>
       </div>
      </Container>
          </div>
    );
    }

}



function mapStateToProps  (state)  {
    return {
      bookNew: state.productBook.productDataBookNew,
    };
}

function mapDispatchToProps (dispatch) {
    return {
        onBookNew:() =>dispatch(getBook.fetchDataFromProductBookNew()) 
    }
}
export default connect (mapStateToProps, mapDispatchToProps)(Home);